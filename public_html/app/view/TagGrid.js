Ext.define('ToDo.view.TagGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'taggrid',
    //controller: 'task',
    
    //reference: 'taggrid',
    
    store: {
        type: 'tags'
    },
    height: 1000,
        
    tbar: [
            { 
                xtype: 'button', 
                text: 'Добавить', 
//                handler: function() {
//                    //Добавляем пустую строку
//                    alert('add');
//                    var newTag = Ext.create('ToDo.model.Tag');
////                    var grid = this.
////                    this.getStore().insert(0, newTask);
//                    Ext.data.StoreManager.lookup('tags').insert(0, newTag);
//                    rowEditing.startEdit(newTag);
//                }
                handler: 'onAdd'
                
            },
            {
                xtype: 'tbseparator'
            }
    ],
    
    columns: [
        {
            dataIndex: 'idTag',
            xtype: 'hiddenfield'
        },
        {
            dataIndex: 'tag',
            header: ' Тег'
        }
    ]
});


