Ext.define('ToDo.view.TaskChartPanel', {
    extend: 'Ext.tab.Panel',
    
    width: 600,
    minWidth: 600,
    height: 400,
    minHeight: 400,
    dtMinCcompletedChart: null,
    dtMaxCompletedChart: null,
    
    DataStore: null,
    
    initComponent: function () {
        var me = this;

        me.PieChartPanel = Ext.create('Ext.panel.Panel', {
            title: 'Состояние задач',
            flex: 1,
            layout: 'fit'
        });   

        me.CompletedBarChartPanel = Ext.create('Ext.panel.Panel', {
            title: 'Выполнение задач',
            flex: 1,
            layout: 'fit'
        });   
        
        me.LoadCharts();
        
        me.items = [
            me.PieChartPanel,
            me.CompletedBarChartPanel
        ];
        
        this.callParent(arguments);
    },
    
    LoadCharts: function() {
        var me = this;
        me.PieChartPanel.add(me.PieChartView());
        me.CompletedBarChartPanel.add(me.CompletedBarChartView());
    },
    
    PieChartView: function () {
        var me = this;
        var chartStore = me.PreparePieChartStore();
        var chart = Ext.create('Ext.chart.Chart', {
            width: 600,
            height: 400,
            store: chartStore,
            legend: {
                position: 'right'
            },

            series: [{
                type: 'pie',
                field: 'count',
                showInLegend: true,
                label: {
                    field: 'groupName'
                },
                tips: {
                    trackMouse: true,
                    layout: 'fit',
                    renderer: function(storeItem, item) {
                        var tipTitle = storeItem.get('groupName') + ': ' + storeItem.get('count');
                        this.setTitle(tipTitle);
                    }
                }
            }]
        });
        return chart;
    },
    
    PreparePieChartStore: function() {
        var completedOnTimeCount = 0; // Количество задач, выполненных в срок
        var overdueCompletedCount = 0; // Количество задач, выполненных позже срока
        var notCompletedOnTimeCount = 0; //Количество невыполненных не просроченных задач (в т.ч. без срока выполнения)
        var overdueNotCompletedCount = 0; // Количество невыполненных просроченных задач
        var today = new Date();
        
        var taskStore = Ext.data.StoreManager.lookup('tasks');
        taskStore.each(function (record) {
            if (record.data.isDone) {
                if (record.data.dtExp && (record.data.dtExp >= record.data.dtDone)) {
                    completedOnTimeCount++;
                }
                else {
                    overdueCompletedCount++;
                }
                
            } else {
                if (record.data.dtExp && (record.data.dtExp >= today)) {
                    overdueNotCompletedCount++;
                } else {
                    notCompletedOnTimeCount++;
                }
            };
        }); 

        return Ext.create('Ext.data.JsonStore', {
            fields: ['groupName','taskCount'],
            data: [
                {groupName: 'Выполненые в срок', count: completedOnTimeCount},
                {groupName: 'Выполненные позже срока', count: overdueCompletedCount},
                {groupName: 'Невыполненные непросроченные', count: notCompletedOnTimeCount},
                {groupName: 'Невыполненные просроченные', count: overdueNotCompletedCount}
            ]
        });
    },

    CompletedBarChartView: function () {
        var me = this;
        var chartStore = me.PrepareCompletedBarChartStore();
        var chart = Ext.create('Ext.chart.Chart', {
            width: 600,
            height: 400,
            store: chartStore,
            axes: [
                {
                    type: 'Time',
                    position: 'bottom',
                    fields: 'dt',
                    dateFormat: 'd.m.y',
                    label: {
                        rotate: {
                            degrees: -44
                        }
                    }                }, 
                {
                    type: 'Numeric',
                    fields: 'count',
                    position: 'left',
                    title: 'Количество выполненных задач'
                }
            ],
            legend: {
                docked: 'bottom'
            },

            series: [{
                type: 'line',
                column: true,
                xField: 'dt',
                yField: 'count',
            }]
        });
        return chart;
    },
    
    PrepareCompletedBarChartStore: function() {
        var me = this;

        var newStoreData = [];
        var needDateRecord;
        me.CompletedBarChartMinDt = null;
        me.CompletedBarChartMaxDt = null;

        function findNeedDateRecord(dt) {
            for (var recIdx = 0; recIdx < newStoreData.length; recIdx++) {
                if ((+newStoreData[recIdx].dt - +dt) === 0) {
                    return newStoreData[recIdx];
                }
            }
            return null;
        };
        
        var taskStore = Ext.data.StoreManager.lookup('tasks');
        taskStore.each(function(record) {
            if (record.data.isDone) {
                needDateRecord = findNeedDateRecord(record.data.dtDone);
                if (needDateRecord) {
                    needDateRecord.count++;
                } 
                else {
                    needDateRecord = {
                        dt: record.data.dtDone,
                        count: 1
                    };
                    newStoreData.push(needDateRecord);
                }
            }
        });
        
        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'dt', 'count'],
            data: newStoreData,
            sorters: [
                {
                    property: 'dt',
                    direction: 'ASC'
                }
            ]
        });
    },
    
    OpenChartWin: function () {
        var me = this;
        
        var newWin = Ext.create('Ext.window.Window', {
            title: 'Диаграммы',
            modal: true,
            layout: 'fit',
            autoScroll: false,
            maximizable: true,
            items: [me]
        });
        newWin.center();
        newWin.show();
    }
});
