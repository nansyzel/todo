Ext.define('ToDo.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'ToDo.view.TaskGrid',
        'ToDo.view.HistoryGrid',
        'ToDo.view.TagGrid',
        'ToDo.view.TaskChartPanel',
        'ToDo.store.Tasks',
        'ToDo.store.Categories',
        'ToDo.store.Tags',
        'ToDo.store.History',
        'ToDo.view.TaskController'
    ],
    
    defaultListenerScope: true,
    scrollable: true,
    items: [
        {
            title: 'Список задач',
            xtype: 'panel',
            layuot: 'fit',
            items: [
                {
                    xtype: 'taskgrid',
                    store: {
                        type: 'tasks'
                    }
                }
           ]
        }
    ]
    
});