var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    saveBtnText: 'Сохранить',
    cancelBtnText: 'Отменить',
    pluginId: 'rowEditing',
    listeners: {
        cancelEdit: 'onRowEditorCancelEdit',
//        validateEdit: 'onRowEditorValidateEdit',
        edit: 'onRowEdit'
    }
});

Ext.define('ToDo.view.TaskGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.taskGrid',
    xtype: 'taskgrid',
    controller: 'task',
    
    reference: 'taskGrid',
    plugins: [ rowEditing ],
        
    store: 'Tasks',
    scrollable: true,
    layout: 'fit',
            
    tbar: [
            { 
                xtype: 'button', 
                text: 'Добавить', 
                action: 'add'
                
            },
            {
                xtype: 'button',
                text: 'Удалить',
                action: 'delete'
                
            },
            {
                xtype: 'tbseparator'
            },
            {
                xtype: 'button',
                text: 'Показать отчет',
                action: 'report'
            },
            {
                xtype: 'button',
                text: 'История',
                action: 'history'
            },
            {
                xtype: 'tbseparator'
            },
            {
                xtype: 'button',
                text: 'Новая категория',
                action: 'newCategory'
            },
            {
                xtype: 'button',
                text: 'Новый тег',
                action: 'newTag'
            }
    ],
    
    columns: [
        {
            dataIndex: 'id',
            hidden: true
        },
        {
            dataIndex: 'text',
            header: 'Задача',
            flex: 10,
            cellWrap: true,
            renderer: function (value, metaData, record, rowIndex, colIndex) {
                    if (record.data.dtDone) {
                        if (record.data.dtExp && (record.data.dtDone > record.data.dtExp)) {
                        // Задача выполнена с опозданием
                            metaData.style = 'background-color: #99FFFF';
                        }
                        else {
                        // Задача выпонена в срок
                            metaData.style = 'background-color: #CCFFCC';
                        }
                    } else {
                        if (record.data.dtExp && record.data.dtExp < (new Date())) {
                        // Задача не выполнена и просрочена
                            metaData.style = 'background-color: #FFCCCC';
                        }
                            
                        
                    }
                   
                    return value;
            },
            editor: {
                xtype: 'textfield',
                allowblank: false
            }
        },
        {
            
            xtype: 'datecolumn',
            header: 'Срок',
            format: 'd.m.y',
            dataIndex: 'dtExp',
            flex: 3,
            editor: {
                xtype: 'datefield',
                allowBlank: true,
                format: 'd.m.y'//
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isDone',
            header: 'Выполнено',
            flex: 1,
            editor: {
                xtype: 'checkbox'
            },
            listeners: {
               checkchange: 'onCheckChangeIsDone'
            }
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'dtDone',
            header: 'Дата выполнения',
            format: 'd.m.y',
            flex: 3,
            editor: {
                xtype: 'datefield',
                allowBlank: true,
                format: 'd.m.y'
            }
        },
        {
            dataIndex: 'category',
            header: 'Категория',
            flex: 4,
            editor: {
                xtype: 'combobox',
                reference: 'categoryCombo',
                store: {
                        type: 'categories'
                },
                allowblank: true,
                editable: false,
                displayField: 'category',
                valueField: 'category',
                queryMode: 'local'
            }
        },
        {
            dataIndex: 'tags',
            header: 'Теги',
            flex: 5,
            editor: {
                xtype: 'tagfield',
                allowblank: true,
                store: {
                        type: 'tags'
                },
                displayField: 'tag',
                valueField: 'tag',
                queryMode: 'local',
                filterPickList: true
                
            }
        }
    ]//,
//    listeners: {
//        select: 'onGridSelect',
//        deselect: 'onGridDeselect'
//    }
});


