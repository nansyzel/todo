Ext.define('ToDo.view.TaskController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.task',       
    
    stores: ['Tasks', 'History'],
    models:  ['Task', 'HistoryRecord'],
    views: ['ToDo.view.TaskGrid'],
    requires: ['ToDo.store.Tasks', 'ToDo.store.History'],
    
    isNewTask: false,
    
    init: function() {
        Ext.create('ToDo.store.History');
        Ext.create('ToDo.store.Categories');
        Ext.create('ToDo.store.Tags');
        this.control({
            'taskGrid button[action=add]' : {
                click: this.onAddClick
            },
            'taskGrid button[action=delete]' : {
                click: this.onDeleteClick
            },
            'taskGrid button[action=report]' : {
                click: this.onReportClick
            },
            'taskGrid button[action=history]' : {
                click: this.onHistoryClick
            },
            'taskGrid button[action=newCategory]' : {
                click: this.onNewCategoryClick
            },
            'taskGrid button[action=newTag]' : {
                click: this.onNewTagClick
            }
        });
    },

    onAddClick: function(button) {
        var me = this;
        var grid = this.getView();
        grid.getPlugin('rowEditing').cancelEdit();

        me.isNewTask = true;
        var newTask = Ext.create('ToDo.model.Task');
        Ext.data.StoreManager.lookup('tasks').insert(0, newTask);

        grid.getPlugin('rowEditing').startEdit(0, 0);
    },
    
    onDeleteClick: function(button) {
        var me = this;
        var grid = me.getView();
        var selRow = grid.getSelection()[0];
        if (selRow) {
            Ext.Msg.show(
                {
                    title:'Удалить?',
                    message: 'Удалить задачу "' + selRow.data.text + '" ?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            Ext.data.StoreManager.lookup('tasks').remove(selRow);                    
                            me.SaveRecordDataToHistory(selRow.data, 2);
                        } 
                    }
                }

            );    
        }
        else {
            Ext.Msg.alert('Ничего не выбрано', 'Выберите запись для удаления', 
                    Ext.emptyFn);
        }
    },
    
    onReportClick: function(button) {
        Ext.create('ToDo.view.TaskChartPanel').OpenChartWin();
    },
    
    onHistoryClick: function(button) {
        var grid = this.getView();
        var newWin = Ext.create('Ext.window.Window', {
            title: 'История изменений задачи',
            modal: true,
            width: 600,
            height: 400,
            layout: 'fit',
            maximizable: true,
            autoScroll: true,
            items: [
                {
                    xtype: 'historygrid'
                }
            ]
        });
        var historyGrid = newWin.items.items[0];
        historyGrid.getStore().clearFilter();

        var selRow = grid.getSelection()[0];
        if (selRow) {
            historyGrid.getStore().filter('idTask', selRow.data.id);
        }
        newWin.center();
        newWin.show();
    },
    
    onNewCategoryClick: function(button) {
        Ext.Msg.prompt('Категория', 'Введите новую категорию:', function(btn, text){
            if (btn == 'ok'){
                var newCatRec = Ext.create('ToDo.model.Category');
                newCatRec.category = text;
                Ext.data.StoreManager.lookup('categories').add(newCatRec);
            }
        });
    },
    
    onNewTagClick: function(button) {
        Ext.Msg.prompt('Тег', 'Введите новый тег:', function(btn, text){
            if (btn == 'ok'){
                var newTagRec = Ext.create('ToDo.model.Tag');
                newTagRec.tag = text;
                Ext.data.StoreManager.lookup('tags').add(newTagRec);
            }
        });
    },
    
    onCheckChangeIsDone: function(cc, index, isChecked, record, e, eOpts) {
        var me = this;
        var taskStore = Ext.data.StoreManager.lookup('tasks');
        var changedRecord = taskStore.getAt(index);
        if (changedRecord) {
            if (isChecked) {
                var today = new Date();
                today.setHours(0,0,0,0);
                changedRecord.set('dtDone', today);
            }
            else {
                changedRecord.set('dtDone', null);
            }
            me.SaveRecordDataToHistory(changedRecord.data, 1);
            taskStore.sync();
        }
    },
    
    onRowEditorCancelEdit: function(rowEditing, context) {
        if (context.record.phantom) {
           context.record.store.remove(context.record);
        }
    },
    
//    onRowEditorValidateEdit: function(editor, e) {
//        var categoryStore = Ext.data.StoreManager.lookup('categories');
//        //Если надо, создаем новую категорию
//        if (e.newValues.category) { 
//            var categoryRecord = categoryStore.findRecord('category', 
//                    e.newValues.category);
//            // Если не нашли новое значение категории среди категорий, 
//            // надо его добавить в хранилище
//            if (!categoryRecord) {
//                categoryRecord = Ext.create('ToDo.model.Category');
//                categoryRecord.category = e.newValues.category;
//                categoryStore.insert(0, categoryRecord);
//                categoryStore.sync();
//            };
//        };
//        return true;
//    },
     
    onRowEdit: function (editor, e) {
        var me = this;
        
        // TODO Если стоит дата выполнения, ставить isDone в true (или дату выполнения сделать readonly)
        // В этом случае убрать редактор у checkcolumn
        
        
        me.SaveRecordDataToHistory(e.record.data, me.isNewTask ? 0 : 1);
        
        me.isNewTask = false;

    },
    
    SaveRecordDataToHistory: function(recdata, operationType) {
        var newHistoryRec = Ext.create('ToDo.model.HistoryRecord');
        newHistoryRec.data.idTask = recdata.id;
        newHistoryRec.data.recordTime = new Date();
        newHistoryRec.data.operationType = operationType;
        // TODO: обходить поля в цикле
        newHistoryRec.data.text = recdata.text;
        newHistoryRec.data.category = recdata.category;
        newHistoryRec.data.isDone = recdata.isDone;
        newHistoryRec.data.dtDone = recdata.dtDone;
        newHistoryRec.data.dtExp = recdata.dtExp;
        newHistoryRec.data.tags = recdata.tags;
       
        var store = Ext.data.StoreManager.lookup('history');
        store.add(newHistoryRec);
        
    }

});
