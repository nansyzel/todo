Ext.define('ToDo.view.HistoryGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'historygrid',
    
    store: {
        type: 'history'
    },
    
    layout: 'fit',
    
    columns: [
        {
            dataIndex: 'idHistory',
            hidden: true
        },
        {
            dataIndex: 'idTask',
            header: 'Ид задачи',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            header:  'Время', 
            dataIndex: 'recordTime',
            format: 'd.m.y h:i:s',
            flex:3
        },
        {
            dataIndex: 'operationType',
            header: 'Тип операции',
            flex: 3,
            renderer:function(v) {
                var operationName = '';
                switch (v) {
                    case 0: operationName = 'Создание'; break;
                    case 1: operationName = 'Редактирование'; break;
                    case 2: operationName = 'Удаление'; break;
                }
                return operationName;
            }
        },
        {
            dataIndex: 'text',
            header: 'Задача',
            flex: 10,
            cellWrap: true
        },
        {
            xtype: 'datecolumn',
            header: 'Срок',
            format: 'd.m.y',
            dataIndex: 'dtExp',
            flex: 3
        },
        {
            xtype: 'booleancolumn',
            dataIndex: 'isDone',
            header: 'Выполнено',
            flex: 1,
            trueText: 'Да',
            falseText: 'Нет'
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'dtDone',
            header: 'Дата выполнения',
            format: 'd.m.y',
            flex: 3
        },
        {
            dataIndex: 'category',
            header: 'Категория',
            flex: 4
        },
        {
            dataIndex: 'tags',
            header: 'Теги',
            flex: 4
        }
    ]
});




