Ext.define('ToDo.store.Tasks', {
    extend: 'Ext.data.Store',
    alias: 'store.tasks',
    storeId: 'tasks',
    requires: ['ToDo.model.Task'],
   
    model: 'ToDo.model.Task',
    
    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'localstorage',
        id: 'taskGrid'
    }    
});