Ext.define('ToDo.store.History', {
    extend: 'Ext.data.Store',
    alias: 'store.history',
    storeId: 'history',
    requires: ['ToDo.model.HistoryRecord'],
   
    model: 'ToDo.model.HistoryRecord',
    
    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'localstorage',
        id: 'historyGrid'
    }    
});
