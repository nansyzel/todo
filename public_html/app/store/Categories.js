Ext.define('ToDo.store.Categories', {
    extend: 'Ext.data.Store',
    alias: 'store.categories',
    storeId: 'categories',
    requires: ['ToDo.model.Category'],

//    model: 'ToDo.model.Category',
    fields: ['category'],
    autoLoad: true,
    autoSync: true,
            
    proxy: {
        type: 'localstorage',
        id: 'categoryGrid'
    }
    
    
});