Ext.define('ToDo.store.Tags', {
    extend: 'Ext.data.Store',
    alias: 'store.tags',
    storeId: 'tags',
    requires: ['ToDo.model.Tag'],
   
//    model: 'Tag',
    fields: ['tag'],
    autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'localstorage',
        id: 'tagGrid'
    }


});
