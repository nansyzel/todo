Ext.define('ToDo.model.Task', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'id', type: 'auto'},// Ид
       {name: 'text', type: 'string'},// Текст задачи
       {name: 'category', type: 'string', reference: 'Category'},
       {name: 'isDone', type: 'bool'},// Выполнено
       {name: 'dtDone', type: 'date'},// Дата выполнения задачи
       {name: 'dtExp', type: 'date'},// Срок выполнения задачи
       {name: 'tags', type: 'string', reference: 'Tag'} //Теги
       
    ],
   
    validations: [
        {
            type: 'length', 
            field: 'text',
            min: 1
        }
       
   ]
});