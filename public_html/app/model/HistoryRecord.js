Ext.define('ToDo.model.HistoryRecord', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'idHistory', type: 'auto'},// Ид записи истории
       {name: 'recordTime', tipe: 'date'}, //Время записи
       //TODO Тип операции задать константами
       {name: 'operationType', type: 'int'},// Тип операции: 0 - создание, 1 - изменение, 2 - удаление задачи
       {name: 'idTask', type: 'int'},//Ид задачи
       {name: 'text', type: 'string'},// Новый текст задачи
       {name: 'category', type: 'string'}, //Новая категория задачи
       {name: 'isDone', type: 'bool'},// Новое значение флага "Выполнено"
       {name: 'dtDone', type: 'date'},// Новая дата выполнения задачи
       {name: 'dtExp', type: 'date'},// Новый срок выполнения задачи
       {name: 'tags', type: 'string', reference: 'Tag'} //Новые теги
       
    ]
});